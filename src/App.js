import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import MyNavbar from './components/MyNavbar';
import { BrowserRouter as Router, Switch, Route, Card } from 'react-router-dom'
import Home from './components/Home/Home';
import Account from './components/Account/Account';
import Video from './components/Video/Video';
import Welcome from './components/Welcome';
import Detail from './components/Home/Detail';
export default function App() {

  const [kid,setKid] = useState([
    {
    id : 1,
    title:"kid1",
    des:"des1",
    image: "https://image.freepik.com/free-vector/flat-children-s-day-kids-with-games-candies_23-2148324112.jpg"
    },
    {
    id : 2,
    title:"kid2",
    des:"des1",
    image: "https://image.freepik.com/free-vector/flat-children-s-day-kids-with-games-candies_23-2148324112.jpg"
    },
    {
    id : 3,
    title:"kid3",
    des:"des1",
    image: "https://image.freepik.com/free-vector/flat-children-s-day-kids-with-games-candies_23-2148324112.jpg"
    },
    {
    id : 4,
    title:"kid1",
    des:"des1",
    image: "https://image.freepik.com/free-vector/flat-children-s-day-kids-with-games-candies_23-2148324112.jpg"
    },
    {
    id : 5,
    title:"kid2",
    des:"des1",
    image: "https://image.freepik.com/free-vector/flat-children-s-day-kids-with-games-candies_23-2148324112.jpg"
    },
    {
    id : 6,
    title:"kid3",
    des:"des1",
    image: "https://image.freepik.com/free-vector/flat-children-s-day-kids-with-games-candies_23-2148324112.jpg"
    }
    ])
  return (
    <div>
      <Router>
        <MyNavbar />
        <Switch>
          <Route>
          <Route exact path='/' render={ ()=><div><Home kid={kid}/></div>} />
          <Route  path='/Account' component={Account} />
          <Route  path='/Video' component={Video} />
          <Route  path='/Welcome' component={Welcome} />
          <Route  path='/Detail/:id' component={Detail}/>
          </Route>
        </Switch>

      </Router>
    </div>
  )
}
