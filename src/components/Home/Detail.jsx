import React from 'react'
import { useParams } from 'react-router'

export default function Detail() {
    let paramet = useParams()
    return (
        <div className="container">
            <h2>Details : {paramet.id}</h2>
        </div>
    )
}
