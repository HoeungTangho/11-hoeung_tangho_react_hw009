import React from 'react'
import { Navbar, Nav,Form,FormControl,Button, Card } from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default function MyNavbar() {
    return (
        <div className="container-fluid">
            <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link as={Link} to="/" href="#home">Home</Nav.Link>
                <Nav.Link as={Link} to="/Video" href="#link">Video</Nav.Link>
                <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                <Nav.Link as={Link} to="/Welcome" href="#link">Welcome</Nav.Link>
                <Nav.Link href="#home">Auth</Nav.Link>
                
                </Nav>
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-success">Search</Button>
                </Form>
            </Navbar.Collapse>
            </Navbar>
        </div>
    )
}
