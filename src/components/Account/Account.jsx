import React from 'react'
import { Route, Switch, Link, useRouteMatch } from 'react-router-dom'
import Home from '../Home/Home';
import ReadID from './ReadID';

export default function Account() {
    // let { path, url } = useRouteMatch();
    let{ path,url}=useRouteMatch()
    return (
        <div className="container">
            <h2>Account</h2>
            <ul>
                <li>
                    <Link to={`${url}/netflix`}>Netflix</Link>
                </li>
                <li>
                    <Link to={`${url}/zillo-group`}>Zillo_Group</Link>
                </li>
                <li>
                    <Link to={`${url}/yahho`}>Yahho</Link>
                </li>
                <li>
                    <Link to={`${url}/modus-create`}>Modus_Create</Link>
                </li>
            </ul>
            <Switch>
                <Route path={`${path}/:id`} component={ReadID} />
            </Switch>
        </div>
    )
}
