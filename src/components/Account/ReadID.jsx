import React from 'react'
import { useParams } from 'react-router'

export default function ReadID() {
    let param = useParams()
    return (
        <div>
            <h3>ID : {param.id} </h3>
        </div>
    )
}
